# vendor_xiaomi_spes-firmware

Firmware images for Redmi Note 11 (spes), to include in custom ROM builds.

**Current version**: fw_spes_miui_SPESGlobal_V13.0.2.0.SGCMIXM_6a19483890_12.0

### How to use?

1. Clone this repo to `vendor/xiaomi/spes-firmware`

2. Include it from `BoardConfig.mk` in device tree:

```
# Firmware
-include vendor/xiaomi/spes-firmware/BoardConfigVendor.mk
```

